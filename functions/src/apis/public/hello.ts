import * as admin from "firebase-admin";
import { RequestFunctionAsync } from "../../core/interfaces.type";
import { Request, Response } from "express";

export const callbackHelloWorld = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            await database.ref('hello_world').child('hello').set('world');
            console.log('[INFO] {callbackHelloWorld} - Hello world sucesso');
            res.status(200).json({
                message: '[INFO] {callbackHelloWorld} - Hello world sucesso',
                status: true
            });
        }
        catch (erro) {
            console.error(new Error('[ERRO] {callbackHelloWorld} - Problemas com o Hello World: ' + erro.message));
            res.status(500).json({
                message: '[ERRO] {callbackHelloWorld} - Problemas com o Hello World: ' + erro.message,
                status: false
            })
        }
    };
};