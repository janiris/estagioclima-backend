import {ErroSistema, RequisitionError, TiposErro} from '../shared/models/requisition-error.model';
import {RequestFunctionPromise, RequestFunctionPromiseError} from './interfaces.type';
import {Response} from 'express';
import {ConstantesRequisicao} from '../shared/constants/requisicao.const';
import * as HttpStatus from 'http-status-codes';

/**
 *
 * Middleware para requisições que não forem identificadas.
 *
 */
export const midErroRequisicaoNaoEncontrada = (): RequestFunctionPromise => {
    return (req, res, _): void => {
        console.error('[ERROR] {Erro} Path não encontrado ', req.url);

        res.status(HttpStatus.UNPROCESSABLE_ENTITY)
            .send(new RequisitionError(
                new ErroSistema(
                    TiposErro.ERRO_NECESSARIO,
                    `Path não encontrado - ${req.url}`
                ),
                false
            ));
    };
};

/**
 *
 * Middleware para problema durante a execução de requisições.
 *
 */
export const midErroProblemaRequisicao = (): RequestFunctionPromiseError => {
    return (err, req, res, _) => {
        const mensagem_erro = '[ERROR] {Erro} Problema durante a requisição - ';
        console.error(mensagem_erro, err.message);

        // Se o erro não tiver um código de status definido, define o mesmo como erro interno do sistema
        if (!err.statusCode) {
            err.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        // Retorna a mensagem de erro
        res.status(err.statusCode)
            .send(
                new RequisitionError(
                    new ErroSistema(
                        TiposErro.ERRO_NECESSARIO,
                        mensagem_erro + err.message
                    ),
                    false
                )
            );
    };
};

/**
 *
 * Retorna ao usuario erro em requisição.
 *
 * @param res Objeto para enviar resposta de uma requisição
 * @param tipoErro Tipo do erro presente na requisição
 * @param mensagem Mensagem de erro
 */
export const enviaErroReq = (res: Response, tipoErro: TiposErro, mensagem: string): void => {
    res.status(HttpStatus.UNPROCESSABLE_ENTITY)
        .type(ConstantesRequisicao.HTTP_TIPO_JSON)
        .send(new RequisitionError(
            new ErroSistema(
                tipoErro,
                mensagem
            ),
            false
        ));
};
