import * as fs from 'fs';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as express from 'express';
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';

import DataSnapshot = admin.database.DataSnapshot;

import { Environment } from './environments/environment';
import { DatabasePaths } from './shared/constants/database.const';
import { minutosToMs } from './core/core.helper';
import { callbackUsuarioAutenticado } from './core/autenticacao.helper';
import {
    midErroProblemaRequisicao,
    midErroRequisicaoNaoEncontrada
} from './core/erros.helper';

import {callbackHelloWorld} from './apis/public/hello';
import {callbackHelloCaixaAlta} from './database/hello';

/**
 * --------------------------------- Configuração ---------------------------------
 */

// Carrega arquivo de ambiente
const env = Environment.getEnvironmentVariables();

// Conta de serviço
const serviceAccount = JSON.parse(
    fs.readFileSync(
        `${__dirname}/../config/${env.firebase.cert_file}.json`,
        'utf8'
    )
);

// Configura o serviço do firebase com os dados obtidos através da cloud function
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: env.firebase.database_url,
    databaseAuthVariableOverride: {
        uid: env.firebase.server_uid
    },
    storageBucket: env.firebase.storage_bucket
});

// Configuração de cors
const corsOptions: cors.CorsOptions = {
    /**
     *
     * Checa a origem do cabeçalho de CORS
     *
     * @param origin Origem presente no cabeçalho
     * @param callback Callback a ser executado após a execução da função
     */
    origin: (origin, callback) => {
        callback(null, true);
    }
};

// Configuração para limitação de número máximo de requisições
const reqLimits = {
    /**
     * Define a janela de tempo em ms para cada evento de limite
     */
    windowMs: minutosToMs(10),

    /**
     * Limita cada IP a úm certo número de requisições
     */
    max: 20
};

// Configuração para limitação de número máximo de requisições em ambiente seguro
const reqLimitsSecure = {
    /**
     * Define a janela de tempo em ms para cada evento de limite
     */
    windowMs: minutosToMs(5),

    /**
     * Limita cada IP a úm certo número de requisições
     */
    max: 30
};

/**
 * --------------------------------- Configuração APIs ---------------------------------
 */

// Inicializa as API's públicas e privadas
const app = express();
const app2 = express();

// Habilita a descoberta do IP do cliente
app.set('trust proxy', true);
app2.set('trust proxy', true);

// Configura o CORS para todos os paths
app.use(cors(corsOptions));
app2.use(cors(corsOptions));

// Configura a ferramenta para limite de requisiçoes
app.use(new rateLimit(reqLimits));
app2.use(new rateLimit(reqLimitsSecure));

// Adiciona algumas funções de middleware para segurança
app.use(helmet());
app2.use(helmet());

// Adiciona suporte a compressão zlib para diminuir e agilizar o tempo de transferência
app.use(compression());
app2.use(compression());

// Adiciona biblioteca body-parser para fazer parser de 'application/json', 'application/x-www-form-urlencoded'
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app2.use(bodyParser.json());
app2.use(bodyParser.urlencoded({ extended: true }));

/**
 * ------------------------------- Eventos Database -------------------------------
 */

export const eventHelloWorld = functions
    .runWith({
        timeoutSeconds: 120,
        memory: '256MB'
    })
    .database.ref(DatabasePaths.HELLO_WORLD)
    .onCreate(callbackHelloCaixaAlta(admin.database()));

 /**
 * -------------------------- API HTTP RESTful - Publica 1 --------------------------
 */

app.post('/v1/hello_world', callbackHelloWorld(admin.database()));

 /**
 * -------------------------- API HTTP RESTful - Publica 2 --------------------------
 */

/**
 * ---------------------------- Cloud Functions -----------------------------------
 */

// Expõe a api publica como uma cloud function
export const api: functions.HttpsFunction = functions
    .runWith({
        timeoutSeconds: 120,
        memory: '256MB'
    })
    .https.onRequest(app);

// Expõe a api segura como uma cloud function
export const api2: functions.HttpsFunction = functions
    .runWith({
        timeoutSeconds: 120,
        memory: '256MB'
    })
    .https.onRequest(app2);