/**
 * Tipos de erro presente no sistema
 */
export enum TiposErro {
    ERRO_DESNECESSARIO,
    ERRO_SERIO_ISSO,
    ERRO_VELHO_PARE,
    ERRO_NECESSARIO,
    ERRO_TEILE,
    ERRO_ZAGA
}

/*
 * Classe erro de sistema, erro
 */
export class ErroSistema {
    tipo: TiposErro;
    mensagem: String;

    /**
     *
     * Constroi o objeto de erro para as requisições
     *
     * @param tipo Tipo de erro
     * @param mensagem Mensagem de erro do sistema
     */
    constructor(tipo, mensagem) {
        this.tipo = tipo;
        this.mensagem = mensagem;
    }
}

/**
 * Erro de requisição
 */
export class RequisitionError {
    /**
     * Verifica se uma dada requisição teve ou não problemas
     */
    sucesso: boolean;

    /**
     * Erro informado pelo sistema
     */
    erro: ErroSistema;

    /**
     *
     * Constroi um objeto do tipo Requisicao com problemas.
     *
     * @param sucesso Status da execucao
     * @param erro Erro informado pelo sistema
     */
    constructor(erro, sucesso = true) {
        this.sucesso = sucesso;
        this.erro = erro;
    }
}