import * as admin from 'firebase-admin';

/**
 *
 * Papeis validos para os usuarios.
 *
 */

export enum PapelUsuario {
    ADMINISTRADOR = 0,
    FUNCIONARIO,
    CLIENTE
}

/**
 *
 * Classe com proprieades de autorização de usuarios da plataforma.
 *
 */
export class AuthorizationProperties {
    role: PapelUsuario;
}

/**
 * Classe com permissões especificas para o sistema
 */
export class DecodedIdTokenAuthorization extends AuthorizationProperties implements admin.auth.DecodedIdToken{
    [key: string]: any;
    aud: string;
    auth_time: number;
    exp: number;
    firebase: { identities: { [p: string]: any }; sign_in_provider: string; [p: string]: any };
    iat: number;
    iss: string;
    sub: string;
    uid: string;
}
